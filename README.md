geoipl
======

A Laravel project created on September 11, 2018.


Intro
=====

The project was developed under Ubuntu 16.04, with Laravel 5.4, and it is a take on the testing technical task located on the root of this project, under the filename 'Technical task PHP Developer - GeoIP database API.pdf':

Interpretation of the domain from the description of the requirements:

1. In terms of entity definition, to store the geoIP reference data, one single table seemed fit, since the data is fetched from a CSV file and its structure is pretty much table-wise.

1. The CSV file doesnt really explain what each particular column represents but after a little analysis, I came to the conclusion the column A and B are respectively the minimum and maximum IP addresses as a range for that particular country.
The columns C and D are the same, but in the decimal long representation of the IP address, and the E and F are respectively the Country affected to any IP address encompassed within the IP range.

Setup
=====

1. Install a webserver (Apache is advised which the development of the project was built upon), with PHP 7.1, and mysql.  
Under:  
`./vhost_example/`  
there is an example of a virtualhost configuration to use for the project.
Otherwise you can just use Laravel's webserver, by executing from the root of the project /check further ahead how to fetch it):  
`php artisan serve`  
It will serve the project under the following localhost address:  
`http://127.0.0.1:8000`

1. A few optional PHP extensions are required for the project to work, such as php-curl, php7.0-zip and php-zip.
From a Linux distribution environment, these can be installed by running the following commands:
`sudo apt-get install php-curl`  
`sudo apt-get install php7.0-zip`  
`sudo apt-get install php-zip`  

1. Clone the Git repository from:  
`git@bitbucket.org:rpmsauron/geoipl.git`

1. Change directory into the root of the project, and, to fetch the vendor folder to get third party resources for Laravel project, run:  
`composer install`

1. Use a REST client like Postman or Insomnia to test REST Requests of the API  
Insomnia is advised since settings and history of Requests for the project are provided inside:  
`./GeoIPL_2018-09-11.json`  
which can be imported into Insomnia onto a workspace.

1. You need to generate an Illuminate encryption key for the Laravel project before it can work, by running:  
`php artisan key:generate -v`

1. A database needs to be setup on mysql.  
An .env file needs to exist on the root of the project. You can copy it from the originally existing .env.example file, and then adapting the configuration values as you need, specifically with attention to the values defined on the Database section.  
These values must match what you have defined in your database and respective connection.


Setup Database Schema
=====================

Migrations need to run in order to set the current schema for the database of the project.

1. Run:  
`php artisan migrate`  
in order to alter the database schema according to the changes required, by running the migrations in created timestamp order.  
The migrations run are also stored in the database itself on a Laravel default table called migrations.


Populate Database
=================

1. To populate the database a command was developed to fetch the data from:  
`http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip`  
The file is fetched into local storage at:  
`./storage`  
it is then uncompressed and its data is loaded onto the database.  
The command must be run from the root of the project, and is:  
`php artisan db:populate`


Tests
=====

1. An integration test was developed for the API endpoint, which can be run on phpunit with:  
`./vendor/bin/phpunit tests/Controller/RangeControllerTest.php --filter testGetCountry`


Contact
=====

Any doubts or questions feel free to contact me at:
`rpmsauron[NO_SPAM] AT gmail COM com`

Rui M. Silva, 2018