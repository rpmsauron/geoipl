<?php

namespace Tests\Controller;

use Tests\TestCase;

class RangeControllerTest extends TestCase
{
    const CONTENT_TYPE_VALUE  = "application/json";

    /**
     * Tests getting Country.
     */
    public function testGetCountry()
    {
        $response = $this->json('GET', '/api/locationByIP?IP=2.16.7.1');

        $expectedResult = '{'
            . '"countrycode":"DE",'
            . '"country":"Germany"'
            . '}';

        $this->assertEquals("200", $response->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE_VALUE, $response->headers->get("Content-Type"));
        $this->assertEquals($expectedResult, $response->getContent());
        $this->assertNotEmpty($response->getContent());
    }
}
