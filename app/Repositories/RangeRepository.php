<?php

namespace App\Repositories;

use App\Models\Range;

/**
 * Range Repository
 */
class RangeRepository
{
    /**
     * Returns the Range that has in its decimal IP range a specific decimal IP value,
     * or returns null in case none exists
     *
     * @param  int          $decimal The decimal IP value to find the country for
     *
     * @return Range|null
     */
    public function getCountry(int $decimal): ?Range
    {
        $result = Range::select(['countrycode', 'country'])
            ->where('ipdecmin', '<=', $decimal)
            ->where('ipdecmax', '>=', $decimal)
            ->first();

        return $result;
    }
}
