<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Range;
use ZipArchive;

class PopulateDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:populate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates DB data from downloaded CSV file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = "http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip";
        $storagePath = "storage/GeoIPCountryCSV.zip";
        $extractionPath = "storage/";
        $extractionFilename = "GeoIPCountryWhois.csv"; //auto-defined from within archive

        $ranges = Range::all()->toArray();
        if (empty($ranges)) {
            $this->info('Database empty. Retrieving data source file from remote server.');
            
            $this->getFile($url, $storagePath);
            $this->info("Uncompressing file.");     
            $archive = new ZipArchive;
            $archiveExists = $archive->open($storagePath);

            if ($archiveExists) {
                $archive->extractTo($extractionPath);
                $archive->close();
                $this->info("File extracted successfully.");
            } else {
                $this->info("Problem occurred on extracting file.");

                return;
            }

            $this->info("Populating database.");
            $this->loadFromSpreadsheet($extractionPath . $extractionFilename);
        } else {
            $this->info("Database was not touched. It already contains data.");
        }
        $this->info("Execution finished");
    }

    /**
     * Downloads data source file
     *
     * @param string $url         The remote file URL
     * @param string $destination The path of the file
     *
     * @return bool Returns true if file exists for download, or false otherwise
     */
    private function getFile(string $url, string $destination)
    {
        $fp = fopen($destination, 'w+');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        if (filesize($destination) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Loads data from spreadsheet file onto the database
     *
     * @param string $spreadsheetPath The path of the spreadsheet file
     *
     * @return bool Returns true if successful import occurred
     */
    private function loadFromSpreadsheet(string $spreadsheetPath)
    {
        $count  = 0;
        $offset = 500;

        $objPHPExcel = \PHPExcel_IOFactory::load($spreadsheetPath);

        $batch = [];
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                foreach ($row->getCellIterator() as $cell) {
                    if (!is_null($cell)) {
                        $column = substr($cell->getCoordinate(), 0, 1);
                        switch ($column) {
                            case "A":
                                $ipmin = $cell->getCalculatedValue();
                                break;
                            case "B":
                                $ipmax = $cell->getCalculatedValue();
                                break;
                            case "C":
                                $ipdecmin = $cell->getCalculatedValue();
                                break;
                            case "D":
                                $ipdecmax = $cell->getCalculatedValue();
                                break;
                            case "E":
                                $countrycode = $cell->getCalculatedValue();
                                break;
                            case "F":
                                $country = $cell->getCalculatedValue();
                                break;
                        }
                    }
                }

                $batch[] = [
                    'ipmin'       => $ipmin,
                    'ipmax'       => $ipmax,
                    'ipdecmin'    => $ipdecmin,
                    'ipdecmax'    => $ipdecmax,
                    'countrycode' => $countrycode,
                    'country'     => $country
                ];

                if ($count % $offset == 0) {
                    Range::insert($batch);
                    $batch = [];
                }
                $count++;
            }
        }

        return true;
    }
}
