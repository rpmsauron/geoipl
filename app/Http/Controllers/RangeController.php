<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\RangeRepository;

class RangeController extends Controller
{
    /**
     * @var CountryRepository
     */
    private $rangeRepository;

    /**
     * Creates a new RangeController.
     *
     * @param RangeRepository $rangeRepository The Range Repository.
     */
    public function __construct(RangeRepository $rangeRepository)
    {
        $this->rangeRepository = $rangeRepository;
    }

    /**
     * Gets a Country from an IP address
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function getCountry(Request $request): Response
    {
        $ipAddress = $request->query->get("IP");
        $decimal = ip2long($ipAddress);
        $country = $this->rangeRepository->getCountry($decimal);

        $response = new Response();
        $response->headers->set("Content-Type", "application/json");
        if (empty($country)) {
            $response->setContent(['error' => 'Not Found']);
            $response->setStatusCode(Response::HTTP_NOT_FOUND);

            return $response;
        }

        $content  = json_encode($country);
        $response->setContent($content);
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }
}
