<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Range extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'ipmin',
        'ipmax',
        'ipdecmin',
        'ipdecmax',
        'countrycode',
        'country'
    ];
}
